#!/bin/bash

US="url-shortener"
DOCKERFILE="./deployment/url-shortener.Dockerfile"

set -e

LINES=`docker ps --all --filter "name=${US}"|wc -l`
if [ ${LINES} -gt 1 ]; then
    docker rm -f ${US}
fi

docker image build -t ${US} -f ${DOCKERFILE} .

docker run -d -p 8080:8080 --name $US $US
