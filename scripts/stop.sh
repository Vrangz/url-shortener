#!/bin/bash

US="url-shortener"

set -e

LINES=`docker ps --all --filter "name=${US}"|wc -l`
if [ ${LINES} -gt 1 ]; then
    docker rm -f ${US}
fi

