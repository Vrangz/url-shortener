package statistics

import "sync"

// Redirects is for management of redirect statistics
type Redirects struct {
	mutex sync.Mutex
	RedirectStatistics
}

// RedirectStatistics contains the redirect statistics
type RedirectStatistics struct {
	RedirectTryCount        int `json:"overall"`
	SuccessfulRedirectCount int `json:"successful"`
}

// NewRedirectStatistics create a new redirect statistics
func NewRedirectStatistics() *Redirects {
	return &Redirects{}
}

// ReportRedirectTry increase count number of redirect tries
func (r *Redirects) ReportRedirectTry() {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	r.RedirectTryCount++
}

// ReportSuccessfulRedirect increase count number of successful redirects
func (r *Redirects) ReportSuccessfulRedirect() {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	r.SuccessfulRedirectCount++
}

// GetStatistics returs the statistics
func (r *Redirects) GetStatistics() RedirectStatistics {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return RedirectStatistics{r.RedirectTryCount, r.SuccessfulRedirectCount}
}
