package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// SetInternalServerError sets 500 code to the context and writes the error in JSON
func SetInternalServerError(c *gin.Context, err error) {
	c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
}
