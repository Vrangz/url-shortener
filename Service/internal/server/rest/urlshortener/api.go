package urlshortener

import (
	"net/http"
	"net/url"
	"urlshortener/internal/log"
	"urlshortener/internal/server/utils"

	"github.com/gin-gonic/gin"
)

// CreateURLPair creates new url pair
func (ctrl *URLShortenerController) CreateURLPair(c *gin.Context) {
	var body struct {
		URL string `json:"url"`
	}

	err := c.BindJSON(&body)
	if err != nil {
		log.PrintErr(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err = url.ParseRequestURI(body.URL)
	if err != nil {
		log.PrintErr(err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	short, err := ctrl.urlPairDatabase.CreateURLPair(body.URL)
	if err != nil {
		log.PrintErr(err)
		utils.SetInternalServerError(c, err)
		return
	}

	shortURL := ctrl.getShortURL(short)
	c.JSON(http.StatusCreated, gin.H{"short": shortURL})
}

// ListURLs lists all url pairs
func (ctrl *URLShortenerController) ListURLs(c *gin.Context) {
	urls, err := ctrl.urlPairDatabase.ListURLs()
	if err != nil {
		log.PrintErr(err)
		utils.SetInternalServerError(c, err)
		return
	}
	c.JSON(http.StatusOK, urls)
}

// DeleteURL deletes url pair of given id in path
func (ctrl *URLShortenerController) DeleteURL(c *gin.Context) {
	id, ok := c.Params.Get("id")
	if !ok {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "no matching param id"})
		return
	}

	if err := ctrl.urlPairDatabase.DeleteURL(id); err != nil {
		log.PrintErr(err)
		utils.SetInternalServerError(c, err)
		return
	}

	c.Writer.WriteHeader(http.StatusNoContent)
}
