package redirector

import (
	"urlshortener/internal/server/statistics"

	"github.com/gin-gonic/gin"
)

// RedirectorController controls all operations related to redirecting
type RedirectorController struct {
	urlPairDatabase urlPairDatabase
	statistics      *statistics.Redirects
}

type urlPairDatabase interface {
	GetURL(short string) (string, error)
}

type redirectorController interface {
	Redirect(c *gin.Context)
}

// NewRedirectorController creates instance of a controller for redirecting
func NewRedirectorController(database urlPairDatabase, statistics *statistics.Redirects) *RedirectorController {
	return &RedirectorController{database, statistics}
}
