package statistician

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"urlshortener/internal/server/statistics"

	"github.com/stretchr/testify/assert"

	"github.com/gin-gonic/gin"
)

func setupRouter() *gin.Engine {
	r := gin.Default()
	ctrl := NewStatisticsController(&statistics.Redirects{})
	r.GET("/api/statistics/redirects", ctrl.GetRedirectStatistics)
	return r
}

func Test_GetRedirectStatistics(t *testing.T) {
	tests := []struct {
		name           string
		wantStatusCode int
		wantBody       string
	}{
		{
			name:           "Testcase1: success",
			wantStatusCode: http.StatusOK,
			wantBody:       `{"overall":0,"successful":0}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := setupRouter()

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/api/statistics/redirects", nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.wantStatusCode, w.Result().StatusCode)
			assert.Equal(t, tt.wantBody, w.Body.String())
		})
	}
}
