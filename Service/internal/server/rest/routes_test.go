package rest

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"urlshortener/internal/database"
	"urlshortener/internal/server/statistics"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

type mockURLShortenerController struct{}
type mockRedirectorController struct{}
type mockStatisticianController struct{}

func (mockURLShortenerController) CreateURLPair(c *gin.Context) {}
func (mockURLShortenerController) ListURLs(c *gin.Context)      {}
func (mockURLShortenerController) DeleteURL(c *gin.Context)     {}

func (mockRedirectorController) Redirect(c *gin.Context) {}

func (mockStatisticianController) GetRedirectStatistics(c *gin.Context) {}

func Test_setupURLShortenerRoutes(t *testing.T) {
	backupURLShortenerControllerFactory := urlShortenerControllerFactory
	defer func() {
		urlShortenerControllerFactory = backupURLShortenerControllerFactory
	}()

	urlShortenerControllerFactory = func(_ string, _ int, _ *database.Database) urlShortenerController {
		return &mockURLShortenerController{}
	}

	tests := []struct {
		name   string
		method string
		route  string
	}{
		{
			name:   "Testcase1: post /api/",
			method: "POST",
			route:  "/api",
		},
		{
			name:   "Testcase2: get /api/",
			method: "GET",
			route:  "/api",
		},
		{
			name:   "Testcase3: delete /api/:id",
			method: "DELETE",
			route:  "/api/id",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := &Server{}
			router := gin.Default()
			server.setupURLShortenerRoutes(router, urlShortenerControllerFactory("", 0, nil))
			w := httptest.NewRecorder()
			req, _ := http.NewRequest(tt.method, tt.route, nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, http.StatusOK, w.Result().StatusCode)
		})
	}
}

func Test_setupRedirectRoutes(t *testing.T) {
	backupURLRedirectorControllerFactory := redirectorControllerFactory
	defer func() {
		redirectorControllerFactory = backupURLRedirectorControllerFactory
	}()

	redirectorControllerFactory = func(_ *database.Database, _ *statistics.Redirects) redirectorController {
		return &mockRedirectorController{}
	}

	tests := []struct {
		name   string
		method string
		route  string
	}{
		{
			name:   "Testcase1: GET /:short",
			method: "GET",
			route:  "/short",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := &Server{}
			router := gin.Default()
			server.setupRedirectRoutes(router, redirectorControllerFactory(nil, nil))
			w := httptest.NewRecorder()
			req, _ := http.NewRequest(tt.method, tt.route, nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, http.StatusOK, w.Result().StatusCode)
		})
	}
}

func Test_setupStatisticsRoutes(t *testing.T) {
	backupStatisticsControllerFactory := statisticsControllerFactory
	defer func() {
		statisticsControllerFactory = backupStatisticsControllerFactory
	}()

	statisticsControllerFactory = func(_ *statistics.Redirects) statisticsController {
		return &mockStatisticianController{}
	}

	tests := []struct {
		name   string
		method string
		route  string
	}{
		{
			name:   "Testcase1: GET /statistics/redirects",
			method: "GET",
			route:  "/api/statistics/redirects",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := &Server{}
			router := gin.Default()
			server.setupStatisticsRoutes(router, statisticsControllerFactory(nil))
			w := httptest.NewRecorder()
			req, _ := http.NewRequest(tt.method, tt.route, nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, http.StatusOK, w.Result().StatusCode)
		})
	}
}