package model

import (
	"urlshortener/internal/generator"

	"github.com/google/uuid"
)

// DBURLPair data model of database record
type DBURLPair struct {
	ID    string `json:"id"`
	Long  string `json:"long"`
	Short string `json:"short"`
}

var shortFactory = func(length int) string {
	return generator.RandLetters(length)
}

// NewDBURLPair creates new object with random id and provided url but short is empty
func NewDBURLPair(url string) *DBURLPair {
	dbURLPair := &DBURLPair{}
	dbURLPair.ID = uuid.New().String()
	dbURLPair.Long = url
	return dbURLPair
}

// RollShort rolls a short of given length and assign it to struct field
func (p *DBURLPair) RollShort(length int) {
	p.Short = shortFactory(length)
}
