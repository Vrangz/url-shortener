package database

import (
	"urlshortener/internal/log"

	"github.com/hashicorp/go-memdb"
)

// Database session
type Database struct {
	client *memdb.MemDB
}

// New creates an instance of database
func New() (*Database, error) {
	db, err := memdb.NewMemDB(getSchema())
	if err != nil {
		log.PrintErr(err)
		return nil, err
	}
	return &Database{db}, nil
}
