package database

import (
	"fmt"
	"net/http"
)

// NotFoundError stores status and reason
type NotFoundError struct {
	Status int
	Reason string
}

func newNotFoundError(reason string) NotFoundError {
	return NotFoundError{Status: http.StatusNotFound, Reason: reason}
}

// Error return string of '<code>: <reason>' format
func (nfe NotFoundError) Error() string {
	return fmt.Sprintf("%d: %s", nfe.Status, nfe.Reason)
}
