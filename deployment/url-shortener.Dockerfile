FROM golang:1.16.4

ENV GOFLAGS="-mod=vendor"

RUN mkdir /Service
RUN mkdir /resources

WORKDIR /Service

COPY Service .
COPY resources/swagger-ui-dist/ /resources/swagger-ui-dist
COPY Service/doc/swagger.yaml /resources/swagger-ui-dist/swagger.yaml

RUN go test ./...

RUN go build -o url-shortener ./cmd/main/main.go

CMD ["./url-shortener"]
